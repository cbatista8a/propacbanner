
    <ul class="list-unstyled  image-list">
        {foreach from=$images item=image key=key}
            <li class="media pt-4 border single-image" data-id="{$image.id}">
                <div class="arrow-area m-3 d-inline-block">
                    <i class="fas fa-arrows-alt arrow-sorting"></i>
                </div>
                <form class="form-save-image col row" method="post" action="{$action_form}">
                    <div class="image-thumbnail col-3 p-3">
                        <img class="border" src="{$image.image_url}" height="250px">
                    </div>
                    <div class="media-body col pt-5 form-group">

                        <input type="hidden" name="id-image" value="{$image.id}">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="image-title-label">{l s='Title' mod='op_orangebanner'}</span>
                                <input type="text" class="form-control" id="image-name-list-{$image.id}" name="image-title" value="{$image.title}" aria-describedby="image-title-label" style="height: auto" required>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="image-subtitle-label">{l s='Subtitle' mod='op_orangebanner'}</span>
                                <textarea class="form-control" id="image-subtitle-list-{$image.id}" name="image-subtitle" aria-labelledby="image-subtitle-label" aria-describedby="image-subtitle-label">{$image.subtitle}</textarea>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" for="button-text">{l s='Text Button' mod='op_orangebanner'}</span>
                                <input type="text" class="form-control" id="button-text-{$image.id}" name="button-text" value="{$image.button_text}" placeholder="{l s='More info' mod='op_orangebanner'}" style="height: auto" required>
                            </div>

                        </div>
                        <div class="input-group mb-3 col-6 row">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="button-link-label">{l s='Button Link' mod='op_orangebanner'}</span>
                                <input type="url" class="form-control" id="button-link-{$image.id}" name="button-link" value="{$image.button_link}" style="height: auto" aria-describedby="button-link-label">
                            </div>
                        </div>
                    </div>
                    <div class="p-5 col-2 form-group" style="height: 250px;">
                        <button type="button" class="btn btn-outline-success btn-block border btn-save-image" name="btn-save-image">{l s='Save' mod='op_orangebanner'}</button>
                        <button type="button" class="btn btn-outline-danger btn-block border btn-delete-image" name="btn-delete-image">{l s='Delete' mod='op_orangebanner'}</button>
                    </div>
                </form>
            </li>
        {/foreach}
        {if count($images) <= 0}
            <span class="caption m-3"><i class="icon icon-file"></i> {l s=' Not images found' mod='op_orangebanner'}</span>
        {/if}
    </ul>
