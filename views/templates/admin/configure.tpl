{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
{*<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>*}
<!------ Include the above in your HEAD tag ---------->

{* This tab show an preview *}
<div class="panel">
	<h3><i class="icon icon-picture"></i> {l s='BannerPix' mod='op_orangebanner'}</h3>
	<div id="refresh">
		<button type="button" class="btn btn-default btn-block border pull-right m-3 col-1 " id="btn-refresh-image"><i class="fas fa-sync " id="ico-refresh"></i> {l s='Refresh' mod='op_orangebanner'}
			<span class="spinner-border spinner-border-sm hidden" id="spinner-refresh" role="status" aria-hidden="true"></span>
			<span class="sr-only">{l s='Loading' mod='op_orangebanner'}...</span>
		</button>
	</div>
	<div class="container">
		<div class="row">
			<h2>{l s='Slider Preview' mod='op_orangebanner'}</h2>
		</div>
		<div id="slider-preview" style="width: 900px;">
			{$slider}
		</div>
	</div>

</div>

{* This tab show an upload form *}
<div class="panel">
	<h3><i class="icon icon-picture"></i> {l s='BannerPix Configuration' mod='op_orangebanner'}</h3>

	<div class="row" id="nav-upload" >

		<form enctype="multipart/form-data" class="col-6" id="form-upload-image" action="{$action_form}" method="POST">
			<div class="">
				<input type="hidden" name="submitCard" value="1">

				<div class="custom-file mb-3 col-6 row">

					<input type="file" class="custom-file-input" id="image-file" name="image-file" accept="image/*">
					<label class="custom-file-label" for="image-file">{l s='Open Image...' mod='op_orangebanner'}</label>
				</div>

				<div class="input-group mb-3 col-6 row">
					<div class="input-group-prepend ">
						<span class="input-group-text" id="image-title-label">{l s='Title' mod='op_orangebanner'}</span>
						<input type="text" class="form-control" id="image-title" name="image-title" aria-describedby="image-title-label" style="height: auto" required>
					</div>
				</div>

				<div class="input-group mb-3 col-6 row">
					<div class="input-group-prepend">
						<span class="input-group-text" id="image-subtitle-label">{l s='Subtitle' mod='op_orangebanner'}</span>
						<textarea class="form-control" id="image-subtitle" name="image-subtitle"  aria-describedby="image-subtitle-label" style="height: auto"></textarea>
					</div>
				</div>

				<div class="input-group mb-3 col-6 row">
					<div class="input-group-prepend">
						<span class="input-group-text" for="button-text">{l s='Text Button' mod='op_orangebanner'}</span>
						<input type="text" class="form-control" id="button-text" name="button-text" placeholder="{l s='More info' mod='op_orangebanner'}" style="height: auto" required>
					</div>
				</div>

				<div class="input-group mb-3 col-6 row">
					<div class="input-group-prepend">
						<span class="input-group-text" for="button-link">{l s='Button Link' mod='op_orangebanner'}</span>
						<input type="url" class="form-control" id="button-link" name="button-link" placeholder="https://your-domain.com/page/" style="height: auto" required>
					</div>
				</div>
			</div>
			<button type="submit" role="submit" class="btn btn-primary pull-left" id="btn-upload-image">{l s='Upload and Save' mod='op_orangebanner'}</button>
		</form>
		<div class="col-6 ">
			<div class="image-thumbnail justify-content-center p-3">
				<img class="border" src="http://placehold.it/250" id="image-thumbnail-file"  height="250px">
			</div>
		</div>

	</div>

</div>

{* This tab show a list of images*}
<div class="panel">
	<h3><i class="icon icon-picture"></i> {l s='Image List' mod='op_orangebanner'}</h3>
	<div id="list-image">
		{$image_list}
	</div>

</div>
