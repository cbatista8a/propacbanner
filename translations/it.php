<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{op_orangebanner}prestashop>op_orangebanner_d98e83b8d47e5fafe981aeca8d6757ef'] = 'Banner personalizzato OrangePix';
$_MODULE['<{op_orangebanner}prestashop>op_orangebanner_3d4a91cddadbe3b96b76ef9c19e6f7a1'] = 'Banner personalizzato per FrontEnd';
$_MODULE['<{op_orangebanner}prestashop>op_orangebanner_04911cf3ac27849e3ed6aefb2e962ad0'] = 'Sei sicuro di disinstallare questo modulo';
$_MODULE['<{op_orangebanner}prestashop>image-list_b78a3223503896721cca1303f776159b'] = 'Titolo';
$_MODULE['<{op_orangebanner}prestashop>image-list_035f4e29da2d6d31303f7d7cfa1be13b'] = 'Sottotitolo';
$_MODULE['<{op_orangebanner}prestashop>image-list_fe136c1adfdca65b8def653c40caa925'] = 'Testo del Pulsante';
$_MODULE['<{op_orangebanner}prestashop>image-list_71948aa4f6e12cdaa5e2e63a5eb8f142'] = 'Più informazioni';
$_MODULE['<{op_orangebanner}prestashop>image-list_c4b4f98bdf14cc79d1cdc449e1cf78df'] = 'Link pulsante';
$_MODULE['<{op_orangebanner}prestashop>image-list_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{op_orangebanner}prestashop>image-list_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
$_MODULE['<{op_orangebanner}prestashop>image-list_b5e76fd1ce8c7dfb859976d7dd43a971'] = 'Non sono state trovate immagini';
$_MODULE['<{op_orangebanner}prestashop>configure_fc3080dbc85437cec73bafa55a96cbd2'] = 'BannerPix';
$_MODULE['<{op_orangebanner}prestashop>configure_63a6a88c066880c5ac42394a22803ca6'] = 'Ricaricare';
$_MODULE['<{op_orangebanner}prestashop>configure_16bfbf9c462762cf1cba4134ec53c504'] = 'Caricamento in corso';
$_MODULE['<{op_orangebanner}prestashop>configure_08df2f1c3ba8c0f92e33eb059a056f62'] = 'Anteprima Banner';
$_MODULE['<{op_orangebanner}prestashop>configure_e2664746f50d537dd42bef61036abc05'] = 'Configurazione BannerPix';
$_MODULE['<{op_orangebanner}prestashop>configure_7d061d852c5e5281d3f0ec98c2fc8ac6'] = 'Apri immagine ...';
$_MODULE['<{op_orangebanner}prestashop>configure_b78a3223503896721cca1303f776159b'] = 'Titolo';
$_MODULE['<{op_orangebanner}prestashop>configure_035f4e29da2d6d31303f7d7cfa1be13b'] = 'Sottotitolo';
$_MODULE['<{op_orangebanner}prestashop>configure_fe136c1adfdca65b8def653c40caa925'] = 'Testo del pulsante';
$_MODULE['<{op_orangebanner}prestashop>configure_71948aa4f6e12cdaa5e2e63a5eb8f142'] = 'Più informazione';
$_MODULE['<{op_orangebanner}prestashop>configure_c4b4f98bdf14cc79d1cdc449e1cf78df'] = 'Link pulsante';
$_MODULE['<{op_orangebanner}prestashop>configure_7f7acbc0d2eb829d6f67c64127b9f17d'] = 'Carica e salva';
$_MODULE['<{op_orangebanner}prestashop>configure_ea59c30e7ee792c0e40b485f19fe8749'] = 'Elenco immagini';
