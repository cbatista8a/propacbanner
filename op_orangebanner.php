<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Op_orangebanner extends Module
{
    protected $config_form = false;
    protected $images_dir;

    public function __construct()
    {
        $this->name = 'op_orangebanner';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'OrangePix';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->images_dir = dirname(__FILE__).'/views/img/banner/';
        $this->displayName = $this->l('OrangePix Custom Banner');
        $this->description = $this->l('Custom Banner for FrontEnd');

        $this->confirmUninstall = $this->l('Are you sure uninstall this module');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');

        // Create image path if not exist
        if (!file_exists($this->images_dir)){
            mkdir($this->images_dir,0777,true);
        }

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayHome');

    }

    public function uninstall()
    {
        //remove images folder
        if (file_exists($this->images_dir)){
            if (PHP_OS === 'Windows')
            {
                exec("rd /s /q {$this->images_dir}");
            }
            else
            {
                exec("rm -rf {$this->images_dir}");
            }
        }

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitCard')) == true) {
            $this->saveNewImageCard();
        }
        if (((bool)Tools::isSubmit('submit-btn-delete-image')) == true) {
            $this->deleteImageCard();
        }
        if (((bool)Tools::isSubmit('submit-btn-save-image')) == true) {
            $this->updateImageCard();
        }
        if (((bool)Tools::isSubmit('submitUpdatePreview')) == true) {
            $this->updateSliderPreviewHTML(true);
        }

        $token = Tools::getAdminTokenLite('AdminModules');
        $action_form = $this->context->link->getAdminLink('AdminModules', false)
          .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token='.$token;
        $this->context->smarty->assign('action_form', $action_form);
        $this->context->smarty->assign('module_dir', $this->_path);

        $this->context->smarty->assign('slider', $this->updateSliderPreviewHTML());
        $this->context->smarty->assign('image_list', $this->getImageListHTML());
        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output;
    }

    /**
     * Save form data.
     */
    public function saveNewImageCard()
    {
        $image= Tools::fileAttachment('image-file');
        if (!file_exists($this->images_dir)){
            mkdir($this->images_dir,0777,true);
        }
        move_uploaded_file($image['tmp_name'], $this->images_dir.$image['rename']);

        $data['title'] = Tools::getValue('image-title');
        $data['image_url'] = $this->_path.'views/img/banner/'.$image['rename'];
        $data['subtitle'] = Tools::getValue('image-subtitle');
        $data['button_text'] = Tools::getValue('button-text');
        $data['button_link'] = Tools::getValue('button-link');

        $result = Db::getInstance()->insert($this->name,$data,false,false,Db::INSERT,false);

        die($this->getImageListHTML());

    }

    /**
     * Get Image List from database
     * @return array|false|mysqli_result|PDOStatement|resource|null
     * @throws PrestaShopDatabaseException
     */
    public function getImageList(){
        $sql = 'SELECT * FROM '.$this->name;
        $images = Db::getInstance()->executeS($sql);

        return $images;
    }

    /**
     * Return image list in HTML template
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws SmartyException
     */
    public function getImageListHTML(){
        $this->context->smarty->assign('images', $this->getImageList());
        $image_list = $this->context->smarty->fetch($this->local_path.'views/templates/admin/image-list.tpl');

        return $image_list;
    }

    /**
     * Update Slider Preview on BackOffice
     * @param bool $update
     *
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws SmartyException
     */
    public function updateSliderPreviewHTML($update=false){
        $this->context->smarty->assign('images', $this->getImageList());
        $slider = $this->context->smarty->fetch($this->local_path.'views/templates/front/slider.tpl');
        if ($update)
            die($slider);

        return $slider;
    }

    /**
     * Delete Image Card
     */
    public function deleteImageCard(){
        $id = Tools::getValue('id-image');
        $result = Db::getInstance()->delete($this->name,'id='.$id,0,false,false);

        die($this->getImageListHTML());
    }

    /**
     * Update Image Card
     */
    public function updateImageCard(){
        $id = Tools::getValue('id-image');

        $data['title'] = Tools::getValue('image-title');
        $data['subtitle'] = Tools::getValue('image-subtitle');
        $data['button_text'] = Tools::getValue('button-text');
        $data['button_link'] = Tools::getValue('button-link');
        $result = Db::getInstance()->update($this->name,$data,'id='.$id,0,false,false,false);

        die($this->getImageListHTML());
    }



    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('configure') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addJS($this->_path.'views/js/bs-custom-file-input.min.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayHome()
    {
        /* Place your code here. */
        $this->updateSliderPreviewHTML();
    }
}
